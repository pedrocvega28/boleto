// Obtener los elementos del formulario
const form = document.getElementById('ticket-form');
const numBoleto = document.getElementById('num-boleto');
const nombreCliente = document.getElementById('nombre-cliente');
const destino = document.getElementById('destino');
const tipoViaje = document.getElementById('tipo-viaje');
const precio = document.getElementById('precio');
const subtotal = document.getElementById('subtotal');
const impuesto = document.getElementById('impuesto');
const total = document.getElementById('total');

// Agregar el evento 'submit' al formulario
form.addEventListener('submit', function(e) {
  // Prevenir el envío del formulario
  e.preventDefault();

  // Obtener los valores de los campos del formulario
  const numBoletoValue = numBoleto.value;
  const nombreClienteValue = nombreCliente.value;
  const destinoValue = destino.value;
  const tipoViajeValue = tipoViaje.value;
  const precioValue = precio.value;

  // Verificar si el tipo de viaje es doble y actualizar el precio
  if (tipoViajeValue === '2') {
    precioValue *= 1.8;
  }

  // Calcular el subtotal, impuesto y total
  const subtotalValue = precioValue;
  const impuestoValue = subtotalValue * 0.16;
  const totalValue = subtotalValue + impuestoValue;

  // Actualizar los campos del formulario con los valores calculados
  subtotal.value = subtotalValue.toFixed(2);
  impuesto.value = impuestoValue.toFixed(2);
  total.value = totalValue.toFixed(2);

  // Mostrar los resultados en la consola
  console.log('Número de Boleto:', numBoletoValue);
  console.log('Nombre del Cliente:', nombreClienteValue);
  console.log('Destino:', destinoValue);
  console.log('Tipo de Viaje:', tipoViajeValue);
  console.log('Precio:', precioValue.toFixed(2));
  console.log('Subtotal:', subtotalValue.toFixed(2));
  console.log('Impuesto:', impuestoValue.toFixed(2));
  console.log('Total:', totalValue.toFixed(2));
});